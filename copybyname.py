#!/usr/bin/env python

#Copyright 2013, Josh Lefler. 
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, version 3.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

#This is a very simple script that takes a source directory
#and copies its contents in alphanumeric order to a destination
#directory.
#
#I wrote this script because the stereo in my Mazda plays files
#from a USB drive in the order they were copied to the disk 
#rather than in alphanumeric order. This makes it easier to 
#reload the disk in proper order when I add new music.
#
#This script will exit if a destination file already exists. It 
#can't simply skip those files as it isn't smart enough to 
#verify the order they were added to the drive and skipping
#them would likely lead to incorrect results.
#
#I just keep a separate "CarMusic" directory that contains symlinks
#to the folders in my primary Music directory that I want
#available in my car. 


import os
import string
import sys
import shutil
import ctypes

def check_args(args):
	if len(args) <> 3:
		print_usage()
		sys.exit()
	
	source = args[1]
	dest = args[2]

	if os.path.isdir(source) == False:
		print "Source is not a directory."
		print_usage()
		sys.exit()

	if os.path.isdir(dest) == False:
		print "Destination is not a directory."
		print_usage()
		sys.exit()
	
	return source, dest

def print_usage():
	#print str(sys.argv)
	print "Usage: ./copybyname.py SourceDirectory DestinationDir"

def iterate_and_copy(source, dest):
	listing = os.listdir(source)
	listing.sort()
	for path in listing:
		fullpath = os.path.join(source, path)
		destfullpath = os.path.join(dest, path)
		if os.path.isdir(fullpath):
			print "Copying (Directory) " + fullpath + " to " + destfullpath
			os.mkdir(destfullpath)
			iterate_and_copy(fullpath, destfullpath)

		if (os.path.isfile(fullpath)):
			print "Copying (File) " + fullpath + " to " + destfullpath
			shutil.copy(fullpath, destfullpath)
			print "Syncing..."
			libc.sync()
			


				



def main():
	source, dest = check_args(sys.argv);
	print "Copying From " + source + " To " + dest
	iterate_and_copy(source, dest)

libc = ctypes.CDLL("libc.so.6")

main()
